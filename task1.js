const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {}
jsonfile.readFile(inputFile,function(err, body){
    console.log("loaded input file content", body);
    output.emails = [];
    for(let i=0; i<body.names.length; i++){
        let str = body.names[i];
        let splitString = str.split('');
        //console.log(splitString);
        let reverseString = splitString.reverse();
        //console.log(reverseString);
        let joinString = reverseString.join('');
        //console.log(body.names[i], joinString);
        joinString = joinString + randomstring.generate(5) + '@gmail.com';
        //console.log(joinString);
        output.emails.push(joinString);
    }
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("All done!");
    });
})